--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

CREATE SCHEMA IF NOT EXISTS devops_metrics_dev_schema;

--
-- Name: dimension; Type: TABLE; Schema: devops_metrics_dev_schema; Owner: tesista
--

CREATE TABLE devops_metrics_dev_schema.dimension (
                                  id integer NOT NULL,
                                  codigo character varying NOT NULL,
                                  descripcion character varying(15) NOT NULL,
                                  peso double precision NOT NULL,
                                  fecha_creacion date DEFAULT now(),
                                  activo boolean DEFAULT true NOT NULL
);


ALTER TABLE devops_metrics_dev_schema.dimension OWNER TO tesista;

--
-- Name: dimension_id_seq; Type: SEQUENCE; Schema: public; Owner: tesista
--

CREATE SEQUENCE devops_metrics_dev_schema.dimension_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE devops_metrics_dev_schema.dimension_id_seq OWNER TO tesista;

--
-- Name: dimension_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tesista
--

ALTER SEQUENCE devops_metrics_dev_schema.dimension_id_seq OWNED BY devops_metrics_dev_schema.dimension.id;


--
-- Name: dimension_practica; Type: TABLE; Schema: public; Owner: tesista
--

CREATE TABLE devops_metrics_dev_schema.dimension_practica (
                                           id integer NOT NULL,
                                           dimension_id integer NOT NULL,
                                           practica_id integer NOT NULL,
                                           fecha_creacion date DEFAULT now() NOT NULL,
                                           activo boolean DEFAULT true NOT NULL
);


ALTER TABLE devops_metrics_dev_schema.dimension_practica OWNER TO tesista;

--
-- Name: dimension_practica_id_seq; Type: SEQUENCE; Schema: public; Owner: tesista
--

CREATE SEQUENCE devops_metrics_dev_schema.dimension_practica_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE devops_metrics_dev_schema.dimension_practica_id_seq OWNER TO tesista;

--
-- Name: dimension_practica_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tesista
--

ALTER SEQUENCE devops_metrics_dev_schema.dimension_practica_id_seq OWNED BY devops_metrics_dev_schema.dimension_practica.id;


--
-- Name: dimension_valor; Type: TABLE; Schema: public; Owner: tesista
--

CREATE TABLE devops_metrics_dev_schema.dimension_valor (
                                        id integer NOT NULL,
                                        dimension_id integer NOT NULL,
                                        valor_id integer NOT NULL,
                                        fecha_creacion date DEFAULT now() NOT NULL,
                                        activo boolean DEFAULT true NOT NULL
);


ALTER TABLE devops_metrics_dev_schema.dimension_valor OWNER TO tesista;

--
-- Name: empresa; Type: TABLE; Schema: public; Owner: tesista
--

CREATE TABLE devops_metrics_dev_schema.empresa (
                                id integer NOT NULL,
                                nombre character varying(50) NOT NULL,
                                fecha_creacion date DEFAULT now() NOT NULL,
                                activo boolean DEFAULT true NOT NULL,
                                descripcion character varying(1000),
                                direccion character varying(100),
                                contacto character varying(30),
                                nit character varying(20) NOT NULL
);


ALTER TABLE devops_metrics_dev_schema.empresa OWNER TO tesista;

--
-- Name: empresa_id_seq; Type: SEQUENCE; Schema: public; Owner: tesista
--

CREATE SEQUENCE devops_metrics_dev_schema.empresa_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE devops_metrics_dev_schema.empresa_id_seq OWNER TO tesista;

--
-- Name: empresa_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tesista
--

ALTER SEQUENCE devops_metrics_dev_schema.empresa_id_seq OWNED BY devops_metrics_dev_schema.empresa.id;


--
-- Name: evaluacion; Type: TABLE; Schema: public; Owner: tesista
--

CREATE TABLE devops_metrics_dev_schema.evaluacion (
                                   id integer NOT NULL,
                                   empresa_id integer NOT NULL,
                                   fecha_creacion date DEFAULT now() NOT NULL,
                                   activo boolean DEFAULT true NOT NULL,
                                   fecha_evaluacion timestamp without time zone NOT NULL,
                                   resultado double precision NOT NULL,
                                   ruta_archivo varchar(300)
);


ALTER TABLE devops_metrics_dev_schema.evaluacion OWNER TO tesista;

--
-- Name: evaluacion_id_seq; Type: SEQUENCE; Schema: public; Owner: tesista
--

CREATE SEQUENCE devops_metrics_dev_schema.evaluacion_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE devops_metrics_dev_schema.evaluacion_id_seq OWNER TO tesista;

--
-- Name: evaluacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tesista
--

ALTER SEQUENCE devops_metrics_dev_schema.evaluacion_id_seq OWNED BY devops_metrics_dev_schema.evaluacion.id;


--
-- Name: ponderacion_practica; Type: TABLE; Schema: public; Owner: tesista
--

CREATE TABLE devops_metrics_dev_schema.ponderacion_practica (
                                             id integer NOT NULL,
                                             tipo_practica_id integer NOT NULL,
                                             fecha_creacion date DEFAULT now() NOT NULL,
                                             activo boolean DEFAULT true NOT NULL,
                                             valor double precision NOT NULL
);


ALTER TABLE devops_metrics_dev_schema.ponderacion_practica OWNER TO tesista;

--
-- Name: ponderacion_practica_id_seq; Type: SEQUENCE; Schema: public; Owner: tesista
--

CREATE SEQUENCE devops_metrics_dev_schema.ponderacion_practica_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE devops_metrics_dev_schema.ponderacion_practica_id_seq OWNER TO tesista;

--
-- Name: ponderacion_practica_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tesista
--

ALTER SEQUENCE devops_metrics_dev_schema.ponderacion_practica_id_seq OWNED BY devops_metrics_dev_schema.ponderacion_practica.id;


--
-- Name: practica; Type: TABLE; Schema: public; Owner: tesista
--

CREATE TABLE devops_metrics_dev_schema.practica (
                                 id integer NOT NULL,
                                 codigo character varying(20) NOT NULL,
                                 descripcion character varying(100) NOT NULL,
                                 tipo_practica_id integer NOT NULL,
                                 fecha_creacion date DEFAULT now() NOT NULL,
                                 activo boolean DEFAULT true,
                                 peso double precision NOT NULL
);


ALTER TABLE devops_metrics_dev_schema.practica OWNER TO tesista;

--
-- Name: practica_id_seq; Type: SEQUENCE; Schema: public; Owner: tesista
--

CREATE SEQUENCE devops_metrics_dev_schema.practica_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE devops_metrics_dev_schema.practica_id_seq OWNER TO tesista;

--
-- Name: practica_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tesista
--

ALTER SEQUENCE devops_metrics_dev_schema.practica_id_seq OWNED BY devops_metrics_dev_schema.practica.id;


--
-- Name: pregunta; Type: TABLE; Schema: public; Owner: tesista
--

CREATE TABLE devops_metrics_dev_schema.pregunta (
                                 id integer NOT NULL,
                                 descripcion character varying NOT NULL,
                                 fecha_creacion date DEFAULT now() NOT NULL,
                                 activo boolean DEFAULT true NOT NULL,
                                 practica_id integer NOT NULL,
                                 codigo character varying(10) NOT NULL
);


ALTER TABLE devops_metrics_dev_schema.pregunta OWNER TO tesista;

--
-- Name: pregunta_id_seq; Type: SEQUENCE; Schema: public; Owner: tesista
--

CREATE SEQUENCE devops_metrics_dev_schema.pregunta_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE devops_metrics_dev_schema.pregunta_id_seq OWNER TO tesista;

--
-- Name: pregunta_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tesista
--

ALTER SEQUENCE devops_metrics_dev_schema.pregunta_id_seq OWNED BY devops_metrics_dev_schema.pregunta.id;


--
-- Name: respuesta_pregunta; Type: TABLE; Schema: public; Owner: tesista
--

CREATE TABLE devops_metrics_dev_schema.respuesta_pregunta (
                                           id integer NOT NULL,
                                           pregunta_id integer NOT NULL,
                                           evaluacion_id integer NOT NULL,
                                           respuesta boolean,
                                           fecha_creacion date DEFAULT now() NOT NULL,
                                           activo boolean DEFAULT true NOT NULL,
                                           observaciones character varying(300)
);


ALTER TABLE devops_metrics_dev_schema.respuesta_pregunta OWNER TO tesista;

--
-- Name: respuesta_pregunta_id_seq; Type: SEQUENCE; Schema: public; Owner: tesista
--

CREATE SEQUENCE devops_metrics_dev_schema.respuesta_pregunta_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE devops_metrics_dev_schema.respuesta_pregunta_id_seq OWNER TO tesista;

--
-- Name: respuesta_pregunta_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tesista
--

ALTER SEQUENCE devops_metrics_dev_schema.respuesta_pregunta_id_seq OWNED BY devops_metrics_dev_schema.respuesta_pregunta.id;


--
-- Name: resultado_dimension; Type: TABLE; Schema: public; Owner: tesista
--

CREATE TABLE devops_metrics_dev_schema.resultado_dimension (
                                            id integer NOT NULL,
                                            dimension_id integer NOT NULL,
                                            evaluacion_id integer NOT NULL,
                                            resultado_individual double precision NOT NULL,
                                            resultado_ponderado double precision NOT NULL,
                                            fecha_creacion date DEFAULT now() NOT NULL,
                                            activo boolean DEFAULT true NOT NULL
);


ALTER TABLE devops_metrics_dev_schema.resultado_dimension OWNER TO tesista;

--
-- Name: resultado_dimension_id_seq; Type: SEQUENCE; Schema: public; Owner: tesista
--

CREATE SEQUENCE devops_metrics_dev_schema.resultado_dimension_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE devops_metrics_dev_schema.resultado_dimension_id_seq OWNER TO tesista;

--
-- Name: resultado_dimension_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tesista
--

ALTER SEQUENCE devops_metrics_dev_schema.resultado_dimension_id_seq OWNED BY devops_metrics_dev_schema.resultado_dimension.id;


--
-- Name: resultado_practica; Type: TABLE; Schema: public; Owner: tesista
--

CREATE TABLE devops_metrics_dev_schema.resultado_practica (
                                           id integer NOT NULL,
                                           practica_id integer NOT NULL,
                                           evaluacion_id integer NOT NULL,
                                           resultado_individual double precision NOT NULL,
                                           resultado_ponderado double precision NOT NULL,
                                           fecha_creacion date DEFAULT now() NOT NULL,
                                           activo boolean DEFAULT true NOT NULL
);


ALTER TABLE devops_metrics_dev_schema.resultado_practica OWNER TO tesista;

--
-- Name: resultado_practica_id_seq; Type: SEQUENCE; Schema: public; Owner: tesista
--

CREATE SEQUENCE devops_metrics_dev_schema.resultado_practica_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE devops_metrics_dev_schema.resultado_practica_id_seq OWNER TO tesista;

--
-- Name: resultado_practica_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tesista
--

ALTER SEQUENCE devops_metrics_dev_schema.resultado_practica_id_seq OWNED BY devops_metrics_dev_schema.resultado_practica.id;


--
-- Name: resultado_valor; Type: TABLE; Schema: public; Owner: tesista
--

CREATE TABLE devops_metrics_dev_schema.resultado_valor (
                                        id integer NOT NULL,
                                        valor_id integer NOT NULL,
                                        evaluacion_id integer NOT NULL,
                                        fecha_creacion date DEFAULT now() NOT NULL,
                                        activo boolean DEFAULT true NOT NULL,
                                        resultado_individual double precision NOT NULL,
                                        resultado_ponderado double precision NOT NULL
);


ALTER TABLE devops_metrics_dev_schema.resultado_valor OWNER TO tesista;

--
-- Name: resultado_valor_id_seq; Type: SEQUENCE; Schema: public; Owner: tesista
--

CREATE SEQUENCE devops_metrics_dev_schema.resultado_valor_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE devops_metrics_dev_schema.resultado_valor_id_seq OWNER TO tesista;

--
-- Name: resultado_valor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tesista
--

ALTER SEQUENCE devops_metrics_dev_schema.resultado_valor_id_seq OWNED BY devops_metrics_dev_schema.resultado_valor.id;


--
-- Name: table_name_id_seq; Type: SEQUENCE; Schema: public; Owner: tesista
--

CREATE SEQUENCE devops_metrics_dev_schema.table_name_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE devops_metrics_dev_schema.table_name_id_seq OWNER TO tesista;

--
-- Name: table_name_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tesista
--

ALTER SEQUENCE devops_metrics_dev_schema.table_name_id_seq OWNED BY devops_metrics_dev_schema.dimension_valor.id;


--
-- Name: tipo_practica; Type: TABLE; Schema: public; Owner: tesista
--

CREATE TABLE devops_metrics_dev_schema.tipo_practica (
                                      id integer NOT NULL,
                                      codigo character varying(1) NOT NULL,
                                      descripcion character varying(30) NOT NULL,
                                      fecha_creacion date DEFAULT now() NOT NULL,
                                      activo boolean DEFAULT true NOT NULL
);


ALTER TABLE devops_metrics_dev_schema.tipo_practica OWNER TO tesista;

--
-- Name: tipo_practica_id_seq; Type: SEQUENCE; Schema: public; Owner: tesista
--

CREATE SEQUENCE devops_metrics_dev_schema.tipo_practica_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE devops_metrics_dev_schema.tipo_practica_id_seq OWNER TO tesista;

--
-- Name: tipo_practica_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tesista
--

ALTER SEQUENCE devops_metrics_dev_schema.tipo_practica_id_seq OWNED BY devops_metrics_dev_schema.tipo_practica.id;


--
-- Name: valor; Type: TABLE; Schema: public; Owner: tesista
--

CREATE TABLE devops_metrics_dev_schema.valor (
                              id integer NOT NULL,
                              codigo character varying(15) NOT NULL,
                              descripcion character varying(20) NOT NULL,
                              peso double precision NOT NULL,
                              fecha_creacion date DEFAULT now() NOT NULL,
                              activo boolean DEFAULT true NOT NULL
);


ALTER TABLE devops_metrics_dev_schema.valor OWNER TO tesista;

--
-- Name: valor_id_seq; Type: SEQUENCE; Schema: public; Owner: tesista
--

CREATE SEQUENCE devops_metrics_dev_schema.valor_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE devops_metrics_dev_schema.valor_id_seq OWNER TO tesista;

--
-- Name: valor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tesista
--

ALTER SEQUENCE devops_metrics_dev_schema.valor_id_seq OWNED BY devops_metrics_dev_schema.valor.id;


--
-- Name: dimension id; Type: DEFAULT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.dimension ALTER COLUMN id SET DEFAULT nextval('devops_metrics_dev_schema.dimension_id_seq'::regclass);


--
-- Name: dimension_practica id; Type: DEFAULT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.dimension_practica ALTER COLUMN id SET DEFAULT nextval('devops_metrics_dev_schema.dimension_practica_id_seq'::regclass);


--
-- Name: dimension_valor id; Type: DEFAULT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.dimension_valor ALTER COLUMN id SET DEFAULT nextval('devops_metrics_dev_schema.table_name_id_seq'::regclass);


--
-- Name: empresa id; Type: DEFAULT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.empresa ALTER COLUMN id SET DEFAULT nextval('devops_metrics_dev_schema.empresa_id_seq'::regclass);


--
-- Name: evaluacion id; Type: DEFAULT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.evaluacion ALTER COLUMN id SET DEFAULT nextval('devops_metrics_dev_schema.evaluacion_id_seq'::regclass);


--
-- Name: ponderacion_practica id; Type: DEFAULT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.ponderacion_practica ALTER COLUMN id SET DEFAULT nextval('devops_metrics_dev_schema.ponderacion_practica_id_seq'::regclass);


--
-- Name: practica id; Type: DEFAULT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.practica ALTER COLUMN id SET DEFAULT nextval('devops_metrics_dev_schema.practica_id_seq'::regclass);


--
-- Name: pregunta id; Type: DEFAULT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.pregunta ALTER COLUMN id SET DEFAULT nextval('devops_metrics_dev_schema.pregunta_id_seq'::regclass);


--
-- Name: respuesta_pregunta id; Type: DEFAULT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.respuesta_pregunta ALTER COLUMN id SET DEFAULT nextval('devops_metrics_dev_schema.respuesta_pregunta_id_seq'::regclass);


--
-- Name: resultado_dimension id; Type: DEFAULT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.resultado_dimension ALTER COLUMN id SET DEFAULT nextval('devops_metrics_dev_schema.resultado_dimension_id_seq'::regclass);


--
-- Name: resultado_practica id; Type: DEFAULT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.resultado_practica ALTER COLUMN id SET DEFAULT nextval('devops_metrics_dev_schema.resultado_practica_id_seq'::regclass);


--
-- Name: resultado_valor id; Type: DEFAULT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.resultado_valor ALTER COLUMN id SET DEFAULT nextval('devops_metrics_dev_schema.resultado_valor_id_seq'::regclass);


--
-- Name: tipo_practica id; Type: DEFAULT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.tipo_practica ALTER COLUMN id SET DEFAULT nextval('devops_metrics_dev_schema.tipo_practica_id_seq'::regclass);


--
-- Name: valor id; Type: DEFAULT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.valor ALTER COLUMN id SET DEFAULT nextval('devops_metrics_dev_schema.valor_id_seq'::regclass);


--
-- Data for Name: dimension; Type: TABLE DATA; Schema: public; Owner: tesista
--

COPY devops_metrics_dev_schema.dimension (id, codigo, descripcion, peso, fecha_creacion, activo) FROM stdin;
53	HERR	Herramientas	0.25	2022-01-10	t
54	PROC	Procesos	0.25	2022-01-10	t
55	CULT	Cultura	0.25	2022-01-10	t
56	PERS	Personas	0.25	2022-01-10	t
\.


--
-- Data for Name: dimension_practica; Type: TABLE DATA; Schema: public; Owner: tesista
--

COPY devops_metrics_dev_schema.dimension_practica (id, dimension_id, practica_id, fecha_creacion, activo) FROM stdin;
598	53	343	2022-01-10	t
599	53	344	2022-01-10	t
600	53	345	2022-01-10	t
601	53	347	2022-01-10	t
602	53	348	2022-01-10	t
603	53	350	2022-01-10	t
604	53	351	2022-01-10	t
605	53	355	2022-01-10	t
606	53	356	2022-01-10	t
607	53	357	2022-01-10	t
608	54	343	2022-01-10	t
609	54	344	2022-01-10	t
610	54	345	2022-01-10	t
611	54	346	2022-01-10	t
612	54	347	2022-01-10	t
613	54	348	2022-01-10	t
614	54	349	2022-01-10	t
615	54	350	2022-01-10	t
616	54	351	2022-01-10	t
617	54	352	2022-01-10	t
618	54	354	2022-01-10	t
619	54	355	2022-01-10	t
620	54	357	2022-01-10	t
621	54	358	2022-01-10	t
622	54	359	2022-01-10	t
623	55	346	2022-01-10	t
624	55	352	2022-01-10	t
625	55	353	2022-01-10	t
626	55	354	2022-01-10	t
627	55	358	2022-01-10	t
628	55	359	2022-01-10	t
629	55	360	2022-01-10	t
630	56	343	2022-01-10	t
631	56	344	2022-01-10	t
632	56	345	2022-01-10	t
633	56	346	2022-01-10	t
634	56	347	2022-01-10	t
635	56	348	2022-01-10	t
636	56	349	2022-01-10	t
637	56	350	2022-01-10	t
638	56	351	2022-01-10	t
639	56	353	2022-01-10	t
640	56	355	2022-01-10	t
641	56	356	2022-01-10	t
642	56	357	2022-01-10	t
643	56	358	2022-01-10	t
644	56	359	2022-01-10	t
645	56	360	2022-01-10	t
\.


--
-- Data for Name: dimension_valor; Type: TABLE DATA; Schema: public; Owner: tesista
--

COPY devops_metrics_dev_schema.dimension_valor (id, dimension_id, valor_id, fecha_creacion, activo) FROM stdin;
57	53	25	2022-01-10	t
58	54	25	2022-01-10	t
59	55	25	2022-01-10	t
60	55	26	2022-01-10	t
61	56	26	2022-01-10	t
62	53	27	2022-01-10	t
63	54	27	2022-01-10	t
64	55	27	2022-01-10	t
65	53	28	2022-01-10	t
66	55	28	2022-01-10	t
67	56	28	2022-01-10	t
\.

--
-- Data for Name: ponderacion_practica; Type: TABLE DATA; Schema: public; Owner: tesista
--

COPY devops_metrics_dev_schema.ponderacion_practica (id, tipo_practica_id, fecha_creacion, activo, valor) FROM stdin;
35	35	2022-01-10	t	0.7
36	36	2022-01-10	t	0.3
\.


--
-- Data for Name: practica; Type: TABLE DATA; Schema: public; Owner: tesista
--

COPY devops_metrics_dev_schema.practica (id, codigo, descripcion, tipo_practica_id, fecha_creacion, activo, peso) FROM stdin;
343	IC	Integración continua	35	2022-01-10	t	0.085
344	EC	Entrega continua	35	2022-01-10	t	0.084
345	PC	Pruebas continuas	35	2022-01-10	t	0.084
346	GR	Gestión de requisitos	35	2022-01-10	t	0.083
347	GD	Gestión de datos	35	2022-01-10	t	0.083
348	SS	Supervisión de la seguridad	35	2022-01-10	t	0.083
349	DE	Dirección estratégica	35	2022-01-10	t	0.083
350	GC	Gestión de la configuración	35	2022-01-10	t	0.083
351	MC	Monitoreo y observabilidad continua	35	2022-01-10	t	0.083
352	ED	Educación entorno a DevOps	35	2022-01-10	t	0.083
353	RC	Realimentación continua	35	2022-01-10	t	0.083
354	MCu	Medición de la cultura	35	2022-01-10	t	0.083
355	DC	Despliegue continuo	36	2022-01-10	t	0.18
356	ICo	Infraestructura como código	36	2022-01-10	t	0.16
357	GA	Gestión de acceso a privilegios	36	2022-01-10	t	0.16
358	AC	Aprendizaje continuo	36	2022-01-10	t	0.16
359	ExC	Experimentación continua	36	2022-01-10	t	0.16
360	SL	Satisfacción laboral	36	2022-01-10	t	0.18
\.


--
-- Data for Name: pregunta; Type: TABLE DATA; Schema: public; Owner: tesista
--

COPY devops_metrics_dev_schema.pregunta (id, descripcion, fecha_creacion, activo, practica_id, codigo) FROM stdin;
1390	¿Se han definido políticas, procedimientos o estrategias explícitas para llevar a cabo el control de versiones en el proyecto?	2022-01-19	t	343	P1_IC
1391	¿Se han definido repositorios que permitan automatizar el control de versiones en los artefactos involucrados durante el proceso de desarrollo?	2022-01-19	t	343	P2_IC
1392	¿El proyecto utiliza herramientas que garanticen la integridad del código fuente antes de realizar actualizaciones de cualquier tipo?	2022-01-19	t	343	P3_IC
1393	¿Existen procedimientos claros para la implementación y documentación de pruebas unitarias?	2022-01-19	t	343	P4_IC
1394	¿La compilación, revisión y despliegue del código fuente está automatizada?	2022-01-19	t	343	P5_IC
1395	¿Existen mecanismos que permitan identificar de manera clara cualquier error durante la integración de nuevo código fuente durante el proceso de desarrollo?	2022-01-19	t	343	P6_IC
1396	¿Existen mecanismos, políticas o procedimientos que permitan la recuperación de un estado estable del sistema en caso de un fallo durante el proceso de integración de nuevo código?	2022-01-19	t	343	P7_IC
1397	¿Los equipos de desarrollo y operaciones tienen acceso al sistema de control de versiones dispuesto para el proyecto?	2022-01-19	t	343	P8_IC
1398	¿El proyecto cuenta con uno o mas ambientes de pre producción?	2022-01-19	t	344	P1_EC
1399	¿El proyecto cuenta con uno o mas ambientes productivos?	2022-01-19	t	344	P2_EC
1400	¿Se han definido políticas, mecanismos o procedimientos claros que permitan identificar cuando y como se puede realizar una entrega a stage, pre producción o producción?	2022-01-19	t	344	P3_EC
1401	¿El proyecto utiliza herramientas que automaticen la entrega de nuevos cambios a pre producción o producción?	2022-01-19	t	344	P4_EC
1402	¿Los ambientes de pre producción y/o producción cuentan con un conjunto de criterios para verificar la integridad de nuevas funcionalidades a liberar?	2022-01-19	t	344	P5_EC
1403	¿El proyecto cuenta con uno o mas ambientes de pruebas (stage)?	2022-01-19	t	345	P1_PC
1404	¿Se llevan a cabo pruebas de integración que garanticen la consistencia del sistema como conjunto?	2022-01-19	t	345	P2_PC
1405	¿El proceso de pruebas incluye escenarios claramente definidos para garantizar la integridad de nuevos cambios aplicados al sistema?	2022-01-19	t	345	P3_PC
1406	¿Se realizan pruebas sobre las API’s?	2022-01-19	t	345	P4_PC
1407	¿Se realizan pruebas para verificar la integridad de los requerimientos no funcionales presentes en el proyecto?	2022-01-19	t	345	P5_PC
1408	¿Las pruebas funcionales son llevadas a cabo siguiendo un plan claramente definido?	2022-01-19	t	345	P6_PC
1409	¿Existen políticas, mecanismos o procedimientos claros para la solución de incidentes identificados durante la etapa de pruebas funcionales y/o no funcionales?	2022-01-19	t	345	P7_PC
1410	¿Se ha definido un espacio común que permita realizar seguimiento al proceso de pruebas?	2022-01-19	t	345	P8_PC
1411	¿Se ha definido un espacio común que permita consultar y obtener información clara sobre los requisitos presentes en el proyecto?	2022-01-19	t	346	P1_GR
1412	¿Se ha definido una estrategia clara para la identificación, documentación y socialización de nuevos requisitos en el proyecto?	2022-01-19	t	346	P2_GR
1413	¿La información de los requisitos es accesible por todas las partes interesadas?	2022-01-19	t	346	P3_GR
1414	¿Se han definido mecanismos que permitan realizar el seguimiento de los requisitos durante el proceso de desarrollo?	2022-01-19	t	346	P4_GR
1415	¿Se han definido mecanismos, estrategias o procedimientos claros para el acceso a los datos?	2022-01-19	t	347	P1_GD
1416	¿Existen políticas para el tratamiento y gestión de datos heredados?	2022-01-19	t	347	P2_GD
1417	¿El proyecto cuenta con un modelo de gestión de datos claro?	2022-01-19	t	347	P3_GD
1418	¿El proyecto cuenta con un diccionario de datos claro y de acceso por todas las partes interesadas?	2022-01-19	t	347	P4_GD
1419	¿El proyecto cuenta con mecanismos para la restauración de versiones anteriores (backups)?	2022-01-19	t	347	P5_GD
1420	¿Existen políticas explícitas sobre el tratamiento de datos?	2022-01-19	t	347	P6_GD
1421	¿Existen mecanismos para el tratamiento de datos sensibles?	2022-01-19	t	347	P7_GD
1422	¿Se han definido mecanismos para alinear las prácticas de seguridad con el proceso de desarrollo?	2022-01-19	t	348	P1_SS
1423	¿Se han definido mecanismos para la identificación de vulnerabilidades de seguridad en el proyecto?	2022-01-19	t	348	P2_SS
1424	¿Se han definido procesos para el tratamiento y solución de vulnerabilidades en el proyecto?	2022-01-19	t	348	P3_SS
1425	¿Se han definido políticas claras y explícitas sobre el uso de herramientas y software de terceros?	2022-01-19	t	348	P4_SS
1426	¿Existen políticas claras sobre la asignación de permisos a las partes involucradas durante el proceso de desarrollo?	2022-01-19	t	348	P5_SS
1427	¿Se han definido políticas de seguridad generales aplicables a todo el proyecto?	2022-01-19	t	348	P6_SS
1428	¿Se han definido políticas para garantizar el control de acceso solo a las partes que lo requieren?	2022-01-19	t	348	P7_SS
1429	¿Se ha definido un plan estratégico?	2022-01-19	t	349	P1_DE
1430	¿Se han definido procesos para la gestión de los objetivos establecidos en el plan estratégico?	2022-01-19	t	349	P2_DE
1431	¿Existen procesos de monitoreo para garantizar que se cumplen los objetivos establecidos en el plan estratégico?	2022-01-19	t	349	P3_DE
1432	¿La empresa conoce su posición actual en el mercado?	2022-01-19	t	349	P4_DE
1433	¿La empresa ha identificado de manera clara su competencia?	2022-01-19	t	349	P5_DE
1434	¿Los objetivos del proyecto están alineados con la visión de la empresa?	2022-01-19	t	349	P6_DE
1435	¿Existe un plan que describa el procedimiento llevado a cabo para realizar la gestión de la configuración en el proyecto?	2022-01-19	t	350	P1_GC
1436	¿Se han identificado de manera clara cada uno de los elementos sensibles a cambio en el proyecto (Ítems de configuración)?	2022-01-19	t	350	P2_GC
1437	¿Se han definido políticas claras para definición de líneas base en el proyecto?	2022-01-19	t	350	P3_GC
1438	¿Existen procedimientos claros para el proceso de solicitud de cambio en el proyecto?	2022-01-19	t	350	P4_GC
1439	¿Se han definido herramientas para automatizar el seguimiento a las solicitudes de cambio en el proyecto?	2022-01-19	t	350	P5_GC
1440	¿Existen políticas para llevar a cabo evaluaciones funcionales sobre los ítems de configuración?	2022-01-19	t	350	P6_GC
1441	¿Existen políticas para llevar a cabo evaluaciones no funcionales sobre los ítems de configuración?	2022-01-19	t	350	P7_GC
1442	¿Se han definido procesos claros para llevar a cabo el seguimiento a los logs implementados en el proyecto?	2022-01-19	t	351	P1_MC
1443	¿Existen mecanismos para realizar el monitoreo de la infraestructura implementada en el proyecto?	2022-01-19	t	351	P2_MC
1444	¿Existen mecanismos para realizar el monitoreo de red?	2022-01-19	t	351	P3_MC
1445	¿Existen mecanismos para monitorear el cumplimiento de aspectos no funcionales en entornos productivos?	2022-01-19	t	351	P4_MC
1446	¿Existen mecanismos para la detección de problemas en ambiente productivo?	2022-01-19	t	351	P5_MC
1447	¿Se han definido espacios para capacitar a las partes interesadas en DevOps?	2022-01-19	t	352	P1_ED
1448	¿Se realiza seguimiento a la forma en la cual las partes interesadas aplican DevOps?	2022-01-19	t	352	P2_ED
1449	¿Todas las partes interesadas saben aplicar DevOps?	2022-01-19	t	352	P3_ED
1450	¿Se han definido espacios para compartir conocimiento?	2022-01-19	t	353	P1_RC
1451	¿Existe apertura a nuevas ideas para mejorar los procesos existentes?	2022-01-19	t	353	P2_RC
1452	¿Se buscan nuevos mecanismos para garantizar que los procesos existentes no se quedan obsoletos?	2022-01-19	t	353	P3_RC
1453	¿Se han definido espacios comunes (wikis) para compartir información de interés común?	2022-01-19	t	353	P4_RC
1454	¿Se han definido espacios de retrospección para identificar aspectos de mejora de manera constante?	2022-01-19	t	353	P5_RC
1455	¿Se han definido mecanismos para medir la cultura organizacional?	2022-01-19	t	354	P1_MCu
1456	¿Se realiza seguimiento para garantizar que existe una cultura organizacional adecuada?	2022-01-19	t	354	P2_MCu
1457	¿Las partes involucradas comprenden y aceptan la cultura presente en la organización?	2022-01-19	t	354	P3_MCu
1458	¿Se han definido procedimientos claros para llevar a cabo planes de acción en caso de ser necesarios?	2022-01-19	t	354	P4_MCu
1459	¿Se han definido procedimientos claros que garanticen la integridad de un artefacto a desplegar?	2022-01-19	t	355	P1_DC
1460	¿Se han definido herramientas para automatizar el despliegue de artefactos?	2022-01-19	t	355	P2_DC
1461	¿Se ha definido un proceso de orquestación que garantice el despliegue de nuevos artefactos?	2022-01-19	t	355	P3_DC
1462	¿El proceso de despliegue requiere que una aprobación manual?	2022-01-19	t	355	P4_DC
1463	¿Existen procedimientos claramente definidos para el aprovisionamiento de la infraestructura necesaria en los proyectos?	2022-01-19	t	356	P1_ICo
1464	¿Se han definido mecanismos para la configuración automatizada de los recursos e infraestructura presente en los proyectos?	2022-01-19	t	356	P2_ICo
1465	¿El proceso de configuración de recursos requiere operaciones manuales?	2022-01-19	t	356	P3_ICo
1466	¿Se han definido mecanismos, estrategias o procedimientos claros para la identificación de privilegios?	2022-01-19	t	357	P1_GA
1467	¿Existen políticas para garantizar que los privilegios son asignados solo a las partes que lo requieran	2022-01-19	t	357	P2_GA
1468	¿Se han definido mecanismos para automatizar la gestión de los privilegios?	2022-01-19	t	357	P3_GA
1469	¿La naturaleza de los privilegios es clara para todas las partes interesadas?	2022-01-19	t	357	P4_GA
1470	¿Se han definido espacios que permitan capacitar a las partes interesadas en temas importantes para la organización?	2022-01-19	t	358	P1_AC
1471	¿Existen espacios que permitan compartir conocimiento?	2022-01-19	t	358	P2_AC
1472	¿Se han definido espacios comunes que permitan centralizar información de interés para todos?	2022-01-19	t	358	P3_AC
1473	¿Existen espacios que permitan identificar aspectos de mejora y planes de acción que permitan recibir realimentación continua?	2022-01-19	t	358	P4_AC
1474	¿Los equipos de trabajo buscan constantemente aprender elementos de interés que permitan mejorar sus procesos existentes?	2022-01-19	t	358	P5_AC
1475	¿La organización fomenta la experimentación con el objetivo de mejorar sus procesos existentes?	2022-01-19	t	359	P1_ExC
1476	¿Existen espacios que permitan identificar nuevas tecnologías/herramientas/procesos?	2022-01-19	t	359	P2_ExC
1477	¿Existen espacios que permitan la implementación de nuevas tecnologías/herramientas/procesos en entornos controlados?	2022-01-19	t	359	P3_ExC
1478	¿Existen mecanismos claros que permitan identificar el nivel de satisfacción laboral en los miembros del equipo de manera objetiva?	2022-01-19	t	360	P1_SL
1479	¿La organización ofrece incentivos que permitan mejorar el nivel de satisfacción laboral?	2022-01-19	t	360	P2_SL
1480	¿El ambiente de trabajo inventiva la comunicación, colaboración y coordinación asertiva?	2022-01-19	t	360	P3_SL
1481	¿Se han definido espacios de realimentación que permitan identificar el grado de satisfacción laboral en el equipo?	2022-01-19	t	360	P4_SL
\.

--
-- Data for Name: tipo_practica; Type: TABLE DATA; Schema: public; Owner: tesista
--

COPY devops_metrics_dev_schema.tipo_practica (id, codigo, descripcion, fecha_creacion, activo) FROM stdin;
35	F	Practica fundamental	2022-01-10	t
36	C	Practica complementaria	2022-01-10	t
\.

--
-- Data for Name: valor; Type: TABLE DATA; Schema: public; Owner: tesista
--

COPY devops_metrics_dev_schema.valor (id, codigo, descripcion, peso, fecha_creacion, activo) FROM stdin;
25	AUT	Automatización	0.25	2022-01-10	t
26	COL	Colaboración	0.25	2022-01-10	t
27	MED	Medición	0.25	2022-01-10	t
28	COM	Comunicación	0.25	2022-01-10	t
\.


--
-- Name: dimension_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tesista
--

SELECT pg_catalog.setval('devops_metrics_dev_schema.dimension_id_seq', 56, true);


--
-- Name: dimension_practica_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tesista
--

SELECT pg_catalog.setval('devops_metrics_dev_schema.dimension_practica_id_seq', 645, true);


--
-- Name: empresa_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tesista
--

SELECT pg_catalog.setval('devops_metrics_dev_schema.empresa_id_seq', 100, true);


--
-- Name: evaluacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tesista
--

SELECT pg_catalog.setval('devops_metrics_dev_schema.evaluacion_id_seq', 123, true);


--
-- Name: ponderacion_practica_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tesista
--

SELECT pg_catalog.setval('devops_metrics_dev_schema.ponderacion_practica_id_seq', 36, true);


--
-- Name: practica_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tesista
--

SELECT pg_catalog.setval('devops_metrics_dev_schema.practica_id_seq', 360, true);


--
-- Name: pregunta_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tesista
--

SELECT pg_catalog.setval('devops_metrics_dev_schema.pregunta_id_seq', 1481, true);


--
-- Name: respuesta_pregunta_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tesista
--

SELECT pg_catalog.setval('devops_metrics_dev_schema.respuesta_pregunta_id_seq', 8128, true);


--
-- Name: resultado_dimension_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tesista
--

SELECT pg_catalog.setval('devops_metrics_dev_schema.resultado_dimension_id_seq', 224, true);


--
-- Name: resultado_practica_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tesista
--

SELECT pg_catalog.setval('devops_metrics_dev_schema.resultado_practica_id_seq', 1008, true);


--
-- Name: resultado_valor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tesista
--

SELECT pg_catalog.setval('devops_metrics_dev_schema.resultado_valor_id_seq', 224, true);


--
-- Name: table_name_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tesista
--

SELECT pg_catalog.setval('devops_metrics_dev_schema.table_name_id_seq', 67, true);


--
-- Name: tipo_practica_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tesista
--

SELECT pg_catalog.setval('devops_metrics_dev_schema.tipo_practica_id_seq', 36, true);


--
-- Name: valor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tesista
--

SELECT pg_catalog.setval('devops_metrics_dev_schema.valor_id_seq', 28, true);


--
-- Name: dimension dimension_pk; Type: CONSTRAINT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.dimension
    ADD CONSTRAINT dimension_pk PRIMARY KEY (id);


--
-- Name: dimension_practica dimension_practica_pk; Type: CONSTRAINT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.dimension_practica
    ADD CONSTRAINT dimension_practica_pk PRIMARY KEY (id);


--
-- Name: empresa empresa_pk; Type: CONSTRAINT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.empresa
    ADD CONSTRAINT empresa_pk PRIMARY KEY (id);


--
-- Name: evaluacion evaluacion_pk; Type: CONSTRAINT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.evaluacion
    ADD CONSTRAINT evaluacion_pk PRIMARY KEY (id);


--
-- Name: ponderacion_practica ponderacion_practica_pk; Type: CONSTRAINT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.ponderacion_practica
    ADD CONSTRAINT ponderacion_practica_pk PRIMARY KEY (id);


--
-- Name: practica practica_pk; Type: CONSTRAINT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.practica
    ADD CONSTRAINT practica_pk PRIMARY KEY (id);


--
-- Name: pregunta pregunta_pk; Type: CONSTRAINT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.pregunta
    ADD CONSTRAINT pregunta_pk PRIMARY KEY (id);


--
-- Name: respuesta_pregunta respuesta_pregunta_pk; Type: CONSTRAINT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.respuesta_pregunta
    ADD CONSTRAINT respuesta_pregunta_pk PRIMARY KEY (id);


--
-- Name: resultado_dimension resultado_dimension_pk; Type: CONSTRAINT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.resultado_dimension
    ADD CONSTRAINT resultado_dimension_pk PRIMARY KEY (id);


--
-- Name: resultado_practica resultado_practica_pk; Type: CONSTRAINT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.resultado_practica
    ADD CONSTRAINT resultado_practica_pk PRIMARY KEY (id);


--
-- Name: resultado_valor resultado_valor_pk; Type: CONSTRAINT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.resultado_valor
    ADD CONSTRAINT resultado_valor_pk PRIMARY KEY (id);


--
-- Name: dimension_valor table_name_pk; Type: CONSTRAINT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.dimension_valor
    ADD CONSTRAINT table_name_pk PRIMARY KEY (id);


--
-- Name: tipo_practica tipo_practica_pk; Type: CONSTRAINT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.tipo_practica
    ADD CONSTRAINT tipo_practica_pk PRIMARY KEY (id);


--
-- Name: valor valor_pk; Type: CONSTRAINT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.valor
    ADD CONSTRAINT valor_pk PRIMARY KEY (id);


--
-- Name: dimension_practica dimension_practica_dimension_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.dimension_practica
    ADD CONSTRAINT dimension_practica_dimension_id_fk FOREIGN KEY (dimension_id) REFERENCES devops_metrics_dev_schema.dimension(id);


--
-- Name: dimension_practica dimension_practica_practica_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.dimension_practica
    ADD CONSTRAINT dimension_practica_practica_id_fk FOREIGN KEY (practica_id) REFERENCES devops_metrics_dev_schema.practica(id);


--
-- Name: evaluacion evaluacion_empresa_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.evaluacion
    ADD CONSTRAINT evaluacion_empresa_id_fk FOREIGN KEY (empresa_id) REFERENCES devops_metrics_dev_schema.empresa(id);


--
-- Name: ponderacion_practica ponderacion_practica_tipo_practica_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.ponderacion_practica
    ADD CONSTRAINT ponderacion_practica_tipo_practica_id_fk FOREIGN KEY (tipo_practica_id) REFERENCES devops_metrics_dev_schema.tipo_practica(id);


--
-- Name: practica practica_tipo_practica_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.practica
    ADD CONSTRAINT practica_tipo_practica_id_fk FOREIGN KEY (tipo_practica_id) REFERENCES devops_metrics_dev_schema.tipo_practica(id);


--
-- Name: pregunta pregunta_practica_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.pregunta
    ADD CONSTRAINT pregunta_practica_id_fk FOREIGN KEY (practica_id) REFERENCES devops_metrics_dev_schema.practica(id);


--
-- Name: respuesta_pregunta respuesta_pregunta_evaluacion_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.respuesta_pregunta
    ADD CONSTRAINT respuesta_pregunta_evaluacion_id_fk FOREIGN KEY (evaluacion_id) REFERENCES devops_metrics_dev_schema.evaluacion(id);


--
-- Name: respuesta_pregunta respuesta_pregunta_pregunta_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.respuesta_pregunta
    ADD CONSTRAINT respuesta_pregunta_pregunta_id_fk FOREIGN KEY (pregunta_id) REFERENCES devops_metrics_dev_schema.pregunta(id);


--
-- Name: resultado_dimension resultado_dimension_dimension_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.resultado_dimension
    ADD CONSTRAINT resultado_dimension_dimension_id_fk FOREIGN KEY (dimension_id) REFERENCES devops_metrics_dev_schema.dimension(id);


--
-- Name: resultado_dimension resultado_dimension_evaluacion_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.resultado_dimension
    ADD CONSTRAINT resultado_dimension_evaluacion_id_fk FOREIGN KEY (evaluacion_id) REFERENCES devops_metrics_dev_schema.evaluacion(id);


--
-- Name: resultado_valor resultado_valor_evaluacion_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.resultado_valor
    ADD CONSTRAINT resultado_valor_evaluacion_id_fk FOREIGN KEY (evaluacion_id) REFERENCES devops_metrics_dev_schema.evaluacion(id);


--
-- Name: resultado_valor resultado_valor_valor_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.resultado_valor
    ADD CONSTRAINT resultado_valor_valor_id_fk FOREIGN KEY (valor_id) REFERENCES devops_metrics_dev_schema.valor(id);


--
-- Name: dimension_valor table_name_dimension_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.dimension_valor
    ADD CONSTRAINT table_name_dimension_id_fk FOREIGN KEY (dimension_id) REFERENCES devops_metrics_dev_schema.dimension(id);


--
-- Name: resultado_practica table_name_evaluacion_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.resultado_practica
    ADD CONSTRAINT table_name_evaluacion_id_fk FOREIGN KEY (evaluacion_id) REFERENCES devops_metrics_dev_schema.evaluacion(id);


--
-- Name: resultado_practica table_name_practica_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.resultado_practica
    ADD CONSTRAINT table_name_practica_id_fk FOREIGN KEY (practica_id) REFERENCES devops_metrics_dev_schema.practica(id);


--
-- Name: dimension_valor table_name_valor_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: tesista
--

ALTER TABLE ONLY devops_metrics_dev_schema.dimension_valor
    ADD CONSTRAINT table_name_valor_id_fk FOREIGN KEY (valor_id) REFERENCES devops_metrics_dev_schema.valor(id);


--
-- PostgreSQL database dump complete
--
