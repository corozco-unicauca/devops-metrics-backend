package com.unicauca.maestria.tesis.app.controller;

import com.unicauca.maestria.tesis.app.entity.Empresa;
import com.unicauca.maestria.tesis.app.exception.DevOpsMetricsException;
import com.unicauca.maestria.tesis.app.service.AssessmentService;
import com.unicauca.maestria.tesis.app.vo.ResultadoEvaluacionVO;
import com.unicauca.maestria.tesis.app.vo.ResumenPreguntasVO;
import com.unicauca.maestria.tesis.app.vo.request.ResumenPreguntasRQ;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/metrics")
@AllArgsConstructor
@Data
@Tag(name = "DevOpsMetricsController", description = "Endpoints related a company evaluation process")
public class DevOpsAssessmentController {

    private final Logger LOGGER = LoggerFactory.getLogger(DevOpsAssessmentController.class);

    private AssessmentService assessmentService;

    @Operation(summary = "Upload a xls file to perform a DevOps Assessment process")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = " Successfully assessment", content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ResultadoEvaluacionVO.class))}), @ApiResponse(responseCode = "400", description = "Bad Request", content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = DevOpsMetricsException.class))}), @ApiResponse(responseCode = "404", description = "Assessment Not Found", content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = DevOpsMetricsException.class))}), @ApiResponse(responseCode = "500", description = "Internal Server Error", content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = DevOpsMetricsException.class))}),})
    @PostMapping(value = "/upload", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResultadoEvaluacionVO uploadFromClient(final MultipartFile file) {
        return assessmentService.upload(file);
    }

    @PostMapping(value = "/create", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public Empresa uploadFromApp(final MultipartFile file) {
        return assessmentService.uploadFromApp(file);
    }

    @GetMapping(value = "/evaluacion/{id}")
    public ResultadoEvaluacionVO getEvaluacion(@PathVariable("id") final Long id) {
        return assessmentService.getResultados(id);
    }

    @GetMapping(value = "/evaluacion/download/{id}")
    public ResponseEntity<Resource> downloadFile(@PathVariable("id") final Long id) {
        return assessmentService.downloadFile(id);
    }

    @PostMapping(value = "/preguntas")
    public List<ResumenPreguntasVO> getPreguntasByPracticaId(@RequestBody final ResumenPreguntasRQ rq) {
        return assessmentService.getPreguntasByPracticaId(rq);
    }

    @PostMapping(value = "/upload/dummy")
    public Empresa uploadDummy() {
        return assessmentService.uploadDummy();
    }
}
