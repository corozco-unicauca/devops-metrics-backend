package com.unicauca.maestria.tesis.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevopsMetricsBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(DevopsMetricsBackendApplication.class, args);
    }

}
