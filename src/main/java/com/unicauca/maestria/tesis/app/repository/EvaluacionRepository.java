package com.unicauca.maestria.tesis.app.repository;

import com.unicauca.maestria.tesis.app.entity.Evaluacion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EvaluacionRepository extends JpaRepository<Evaluacion, Long> {

    @Query(value = "SELECT v FROM evaluacion v JOIN v.empresa e WHERE v.id =:id")
    Optional<Evaluacion> findById(Long id);

    @Query(value = "SELECT v FROM evaluacion v WHERE v.empresa.id =:id ORDER BY v.id DESC")
    List<Evaluacion> findByEmpresaId(Long id);
}
