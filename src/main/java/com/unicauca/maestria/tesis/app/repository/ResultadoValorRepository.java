package com.unicauca.maestria.tesis.app.repository;

import com.unicauca.maestria.tesis.app.entity.ResultadoValor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ResultadoValorRepository extends JpaRepository<ResultadoValor, Long> {

    @Query(value = "SELECT rv FROM resultado_valor rv JOIN rv.evaluacion e" +
            " JOIN rv.valor v WHERE e.id=:idEvaluacion AND rv.activo = true ORDER BY v.id ASC")
    List<ResultadoValor> findResultados(Long idEvaluacion);
}
