package com.unicauca.maestria.tesis.app.controller;

import com.unicauca.maestria.tesis.app.entity.Empresa;
import com.unicauca.maestria.tesis.app.exception.AdminException;
import com.unicauca.maestria.tesis.app.service.AssessmentService;
import com.unicauca.maestria.tesis.app.service.CompanyService;
import com.unicauca.maestria.tesis.app.vo.EvaluacionVO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/admin")
@AllArgsConstructor
@Data
@Tag(name = "AdminController", description = "Endpoints related with the Company and Evaluations information")
public class AdminController {

    private final Logger LOGGER = LoggerFactory.getLogger(AdminController.class);

    private CompanyService companyService;
    private AssessmentService assessmentService;

    private static final String COMPANIES_ENDPOINT = "/empresas";
    private static final String DETAIL_ENDPOINT = "/detalle";

    @Operation(summary = "Get all the active companies")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Companies information", content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = Empresa.class))}), @ApiResponse(responseCode = "400", description = "Bad Request", content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = AdminException.class))}), @ApiResponse(responseCode = "404", description = "Company Not Found", content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = AdminException.class))}), @ApiResponse(responseCode = "500", description = "Internal Server Error", content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = AdminException.class))}),})
    @GetMapping(value = COMPANIES_ENDPOINT)
    public ResponseEntity<List<Empresa>> findAllActive() {
        LOGGER.info("AdminController#findAllActive - START");
        final List<Empresa> response = companyService.findAllActive();
        LOGGER.info("AdminController#findAllActive - END");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Operation(summary = "Find a company given the id")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Company information", content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = Empresa.class))}), @ApiResponse(responseCode = "400", description = "Bad Request", content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = AdminException.class))}), @ApiResponse(responseCode = "404", description = "Company Not Found", content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = AdminException.class))}), @ApiResponse(responseCode = "500", description = "Internal Server Error", content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = AdminException.class))}),})
    @GetMapping(value = DETAIL_ENDPOINT + "/{id}")
    public ResponseEntity<Empresa> findById(@PathVariable("id") final Long id) {
        LOGGER.info("AdminController#findById - START - id={}", id);
        final Empresa response = companyService.findById(id);
        LOGGER.info("AdminController#findById - END");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Operation(summary = "Find all the evaluations related to an specific company id")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Retrieve evaluations information", content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = EvaluacionVO.class))}), @ApiResponse(responseCode = "400", description = "Bad Request", content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = AdminException.class))}), @ApiResponse(responseCode = "404", description = "Evaluations not found", content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = AdminException.class))}), @ApiResponse(responseCode = "500", description = "Internal Server Error", content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = AdminException.class))}),})
    @GetMapping(value = DETAIL_ENDPOINT + "/evaluaciones/{id}")
    public ResponseEntity<List<EvaluacionVO>> findByCompanyId(@PathVariable("id") final Long id) {
        LOGGER.info("AdminController#findByEmpresaId - START - id={}", id);
        final List<EvaluacionVO> response = assessmentService.findByCompanyId(id);
        LOGGER.info("AdminController#findByEmpresaId - END");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
