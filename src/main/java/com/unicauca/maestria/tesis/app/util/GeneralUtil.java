package com.unicauca.maestria.tesis.app.util;

import org.apache.commons.math.util.MathUtils;

public final class GeneralUtil {

    private static final int GLOBAL_ROUND_VALUE = 2;

    private static double round(final double value) {
        return MathUtils.round(value, GeneralUtil.GLOBAL_ROUND_VALUE);
    }

    public static double decToPercentaje(final double value) {
        return round(value);
    }

    public static String getGradoCumplimiento(final double res) {
        double percentaje = decToPercentaje(res);
        if (percentaje >= 0 && percentaje <= 15) {
            return "No alcanzado";
        } else if (percentaje > 15 && percentaje <= 50) {
            return "Parcialmente alcanzado";
        } else if (percentaje > 50 && percentaje <= 85) {
            return "Ampliamente alcanzado";
        } else {
            return "Totalmente alcanzado";
        }
    }

}
