package com.unicauca.maestria.tesis.app.entity;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@Entity(name = "valor")
public class Valor extends EntidadCodigo {

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "peso")
    private float peso;

    public Valor() {
        super();
    }
}
