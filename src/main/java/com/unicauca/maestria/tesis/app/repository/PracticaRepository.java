package com.unicauca.maestria.tesis.app.repository;

import com.unicauca.maestria.tesis.app.entity.Practica;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PracticaRepository extends JpaRepository<Practica, Long> {

    List<Practica> findByTipoPractica(String tipoPractica);
}
