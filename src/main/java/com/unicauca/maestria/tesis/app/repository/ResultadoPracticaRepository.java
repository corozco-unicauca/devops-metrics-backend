package com.unicauca.maestria.tesis.app.repository;

import com.unicauca.maestria.tesis.app.entity.ResultadoPractica;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ResultadoPracticaRepository extends JpaRepository<ResultadoPractica, Long> {

    @Query(value = "SELECT rp FROM resultado_practica rp JOIN rp.evaluacion e" +
            " JOIN rp.practica p JOIN p.tipoPractica tp " +
            " WHERE e.id=:idEvaluacion AND rp.activo = true ORDER BY p.id ASC, tp.codigo ASC")
    List<ResultadoPractica> findResultados(Long idEvaluacion);
}
