package com.unicauca.maestria.tesis.app.repository;

import com.unicauca.maestria.tesis.app.entity.Empresa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmpresaRepository extends JpaRepository<Empresa, Long> {

    @Query(value = "SELECT e FROM empresa e WHERE e.activo = true")
    List<Empresa> findActivos();

    @Query(value = "SELECT e FROM empresa e WHERE e.activo = true AND e.nit =:nit")
    Empresa findByNit(final String nit);
}
