package com.unicauca.maestria.tesis.app.repository;

import com.unicauca.maestria.tesis.app.entity.Pregunta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PreguntaRepository extends JpaRepository<Pregunta, Long> {

    @Query(value = "SELECT p FROM pregunta p JOIN p.practica pr JOIN pr.tipoPractica tp where p.codigo =:codigo")
    Pregunta findByCodigo(String codigo);
}
