package com.unicauca.maestria.tesis.app.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.unicauca.maestria.tesis.app.entity.Empresa;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@Builder
public class ResultadoEvaluacionVO implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonIgnore
    private Empresa empresa;

    private Long idEvaluacion;

    private Long idEmpresa;

    private String nombreEmpresa;

    private Date fechaEvaluacion;

    private List<ResultadoPracticaVO> resultadoPracticas;

    private double resPracticasFund;

    private double resPracticasComp;

    private double resTotalPracticas;

    private List<ResultadoDimensionVO> resultadoDimensiones;

    private double resTotalDimensiones;

    private List<ResultadoValorVO> resultadoValores;

    private double cumplimientoDevOps;

}
