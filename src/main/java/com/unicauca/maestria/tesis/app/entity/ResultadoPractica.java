package com.unicauca.maestria.tesis.app.entity;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@Entity(name = "resultado_practica")
public class ResultadoPractica extends EntidadGeneral {

    @JoinColumn(name = "practica_id", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Practica practica;

    @JoinColumn(name = "evaluacion_id", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Evaluacion evaluacion;

    @Column(name = "resultado_individual")
    private double resultadoIndividual;

    @Column(name = "resultado_ponderado")
    private double resultadoPonderado;

    public ResultadoPractica() {
        super();
    }
}
