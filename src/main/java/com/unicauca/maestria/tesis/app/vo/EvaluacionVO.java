package com.unicauca.maestria.tesis.app.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EvaluacionVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date fechaEvaluacion;

    private String resultadoEvaluacion;

    private String gradoCumplimiento;

    private Date fechaCreacion;

    private String rutaArchivo;
}
