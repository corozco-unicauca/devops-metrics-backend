package com.unicauca.maestria.tesis.app.entity;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@Entity(name = "practica")
public class Practica extends EntidadCodigo {

    @Column(name = "descripcion")
    private String descripcion;

    @JoinColumn(name = "tipo_practica_id", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private TipoPractica tipoPractica;

    @Column(name = "peso")
    private float peso;

    public Practica() {
        super();
    }
}
