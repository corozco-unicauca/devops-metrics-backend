package com.unicauca.maestria.tesis.app.entity;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@Entity(name = "ponderacion_practica")
public class PonderacionPractica extends EntidadGeneral {

    @JoinColumn(name = "tipo_practica_id", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private TipoPractica tipoPractica;

    @Column(name = "valor")
    private float valor;

    public PonderacionPractica() {
        super();
    }
}
