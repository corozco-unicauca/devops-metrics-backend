package com.unicauca.maestria.tesis.app.service;

import com.unicauca.maestria.tesis.app.entity.Empresa;
import com.unicauca.maestria.tesis.app.vo.EvaluacionVO;
import com.unicauca.maestria.tesis.app.vo.ResultadoEvaluacionVO;
import com.unicauca.maestria.tesis.app.vo.ResumenPreguntasVO;
import com.unicauca.maestria.tesis.app.vo.request.ResumenPreguntasRQ;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public interface AssessmentService {

    List<EvaluacionVO> findByCompanyId(Long id);

    Empresa uploadFromApp(MultipartFile file);

    ResultadoEvaluacionVO upload(MultipartFile file);

    ResultadoEvaluacionVO getResultados(Long id);

    ResponseEntity<Resource> downloadFile(Long id);

    List<ResumenPreguntasVO> getPreguntasByPracticaId(ResumenPreguntasRQ rq);

    Empresa uploadDummy();

}
