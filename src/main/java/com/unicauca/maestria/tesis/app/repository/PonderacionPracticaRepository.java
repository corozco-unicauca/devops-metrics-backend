package com.unicauca.maestria.tesis.app.repository;

import com.unicauca.maestria.tesis.app.entity.PonderacionPractica;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PonderacionPracticaRepository extends JpaRepository<PonderacionPractica, Long> {

    @Query(value = "SELECT pp FROM ponderacion_practica pp JOIN pp.tipoPractica WHERE pp.activo = true")
    List<PonderacionPractica> findActivos();
}
