package com.unicauca.maestria.tesis.app.service;

import com.unicauca.maestria.tesis.app.entity.Empresa;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CompanyService {

    List<Empresa> findAllActive();

    Empresa findById(Long id);
}
