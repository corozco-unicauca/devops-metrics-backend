package com.unicauca.maestria.tesis.app.entity;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@Entity(name = "pregunta")
public class Pregunta extends EntidadCodigo {

    @Column(name = "descripcion")
    private String descripcion;

    @JoinColumn(name = "practica_id", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Practica practica;

    public Pregunta() {
        super();
    }
}
