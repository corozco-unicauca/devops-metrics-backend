package com.unicauca.maestria.tesis.app.exception;

/**
 * DevOpsMetricsException
 * Thrown when there's an error related with the DevOpsMetricsController
 */
public class DevOpsMetricsException extends RuntimeException {

    private static final long serialVersionUID = 1L;

}