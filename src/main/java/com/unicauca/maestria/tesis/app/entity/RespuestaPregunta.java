package com.unicauca.maestria.tesis.app.entity;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@Entity(name = "respuesta_pregunta")
public class RespuestaPregunta extends EntidadGeneral {

    @JoinColumn(name = "pregunta_id", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Pregunta pregunta;

    @JoinColumn(name = "evaluacion_id", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Evaluacion evaluacion;

    @Column(name = "respuesta")
    private Boolean respuesta;

    @Column(name = "observaciones")
    private String observaciones;

    public RespuestaPregunta() {
        super();
    }
}
