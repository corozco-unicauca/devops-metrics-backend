package com.unicauca.maestria.tesis.app.service.impl;

import com.unicauca.maestria.tesis.app.entity.*;
import com.unicauca.maestria.tesis.app.repository.*;
import com.unicauca.maestria.tesis.app.service.AssessmentService;
import com.unicauca.maestria.tesis.app.service.S3BucketClientService;
import com.unicauca.maestria.tesis.app.util.GeneralUtil;
import com.unicauca.maestria.tesis.app.vo.*;
import com.unicauca.maestria.tesis.app.vo.request.ResumenPreguntasRQ;
import lombok.AllArgsConstructor;
import lombok.Data;
import one.util.streamex.StreamEx;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Service
@Transactional
@AllArgsConstructor
@Data
public class AssessmentServiceImpl implements AssessmentService {

    private PreguntaRepository preguntaRepository;
    private PonderacionPracticaRepository ponderacionPracticaRepository;
    private DimensionPracticaRepository dimensionPracticaRepository;
    private DimensionValorRepository dimensionValorRepository;
    private EvaluacionRepository evaluacionRepository;
    private ResultadoPracticaRepository resultadoPracticaRepository;
    private ResultadoDimensionRepository resultadoDimensionRepository;
    private ResultadoValorRepository resultadoValorRepository;
    private EmpresaRepository empresaRepository;
    private RespuestaPreguntaRepository respuestaPreguntaRepository;

    private S3BucketClientService s3Client;
    private EntityManager em;

    private static final Logger logger = LoggerFactory.getLogger(AssessmentServiceImpl.class);

    @Override
    public List<EvaluacionVO> findByCompanyId(final Long id) {
        return evaluacionRepository.findByEmpresaId(id).stream().map(aux -> EvaluacionVO.builder().id(aux.getId()).rutaArchivo(aux.getRutaArchivo()).fechaEvaluacion(aux.getFechaEvaluacion()).resultadoEvaluacion(String.valueOf(GeneralUtil.decToPercentaje(100 * aux.getResultado()))).gradoCumplimiento(GeneralUtil.getGradoCumplimiento(100 * aux.getResultado())).fechaCreacion(aux.getFechaCreacion()).build()).collect(Collectors.toList());
    }

    @Override
    public Empresa uploadFromApp(MultipartFile file) {
        return upload(file).getEmpresa();
    }

    @Override
    public ResultadoEvaluacionVO upload(MultipartFile file) {
        try {
            final XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
            final Evaluacion evaluacion = uploadGeneralInfo(workbook);

            final String empresaFolder = evaluacion.getEmpresa().getNit();

            if (!s3Client.folderExists(empresaFolder)) {
                s3Client.createFolder(empresaFolder);
            }

            final String filePath = empresaFolder + "/" + file.getName();
            if (s3Client.fileExists(filePath)) {
                logger.info("EvaluacionServiceImpl#upload - ERROR - fileName already exist {}.", filePath);
                return null;
            }

            s3Client.upload(filePath, file);

            evaluacion.setRutaArchivo(filePath);
            final List<PonderacionPractica> pesoPracticas = ponderacionPracticaRepository.findActivos();

            double pesoFund = pesoPracticas.stream().filter(aux -> "F".equals(aux.getTipoPractica().getCodigo())).findFirst().get().getValor();
            double pesoComp = pesoPracticas.stream().filter(aux -> "C".equals(aux.getTipoPractica().getCodigo())).findFirst().get().getValor();

            List<ResultadoPracticaVO> resultadoPracticas = uploadPracticas(workbook, evaluacion);
            double pft = resultadoPracticas.stream().filter(practica -> practica.getTipoPractica().equals("F")).mapToDouble(ResultadoPracticaVO::getPuntuacionPonderada).sum();
            double pct = resultadoPracticas.stream().filter(practica -> practica.getTipoPractica().equals("C")).mapToDouble(ResultadoPracticaVO::getPuntuacionPonderada).sum();
            double ptcp = (pesoFund * pft) + (pesoComp * pct);

            logger.info("pft:  " + pft + "%");
            logger.info("pct:  " + pct + "%");
            logger.info("PTCP:  " + ptcp + "%");

            List<ResultadoDimensionVO> resultadoDimensiones = uploadDimensiones(evaluacion, resultadoPracticas, pesoFund, pesoComp);
            double pcdt = resultadoDimensiones.stream().mapToDouble(ResultadoDimensionVO::getPuntuacionPonderada).sum();

            logger.info("pcdt:  " + pcdt + "%");

            List<ResultadoValorVO> resultadoValores = uploadValores(evaluacion, resultadoDimensiones);
            double pc_dev = resultadoValores.stream().mapToDouble(ResultadoValorVO::getPuntuacionPonderada).sum();
            logger.info("pc_dev:  " + pc_dev + "%");

            evaluacion.setResultado(pc_dev);
            return ResultadoEvaluacionVO.builder().empresa(evaluacion.getEmpresa()).nombreEmpresa(evaluacion.getEmpresa().getNombre()).fechaEvaluacion(evaluacion.getFechaEvaluacion()).resPracticasFund(pft).resPracticasComp(pct).resTotalPracticas(ptcp).resultadoPracticas(resultadoPracticas).resTotalDimensiones(pcdt).resultadoDimensiones(resultadoDimensiones).resultadoValores(resultadoValores).cumplimientoDevOps(pc_dev).build();
        } catch (IOException e) {
            return null;
        }
    }

    private Evaluacion uploadGeneralInfo(final XSSFWorkbook workbook) {
        final XSSFSheet worksheet = workbook.getSheetAt(0);
        final Stream<Row> rowStream = StreamSupport.stream(worksheet.spliterator(), false);

        final Map<String, String> mapEmpresa = new HashMap<>();
        rowStream.forEach(row -> {
            Stream<Cell> cellStream = StreamSupport.stream(row.spliterator(), false);
            List<String> cellString = cellStream.map(cell -> {
                cell.setCellType(CellType.STRING);
                return cell.getStringCellValue();
            }).collect(Collectors.toList());
            mapEmpresa.put(cellString.get(0), cellString.get(1));
        });

        Empresa e = empresaRepository.findByNit(mapEmpresa.get("Nit"));

        if (null == e) {
            e = Empresa.builder().nombre(mapEmpresa.get("Empresa")).nit(mapEmpresa.get("Nit")).build();
        }

        e.setDireccion(mapEmpresa.get("Dirección"));
        e.setDescripcion(mapEmpresa.get("Descripción"));
        e.setContacto(mapEmpresa.get("Número de contacto"));

        final Empresa savedEmpresa = em.merge(e);

        final Date fechaEvaluacion = DateUtil.getJavaDate(Double.parseDouble(mapEmpresa.get("Fecha")));
        final Evaluacion eval = Evaluacion.builder().empresa(savedEmpresa).fechaEvaluacion(fechaEvaluacion).build();
        return em.merge(eval);
    }

    private List<ResultadoPracticaVO> uploadPracticas(XSSFWorkbook workbook, Evaluacion evaluacion) {
        final List<ResultadoPracticaVO> evaluacionPracticas = new ArrayList<>();
        for (int i = 1; i < workbook.getNumberOfSheets(); i++) {
            final XSSFSheet worksheet = workbook.getSheetAt(i);
            logger.info("hoja " + i + " " + worksheet.getSheetName());

            final Stream<Row> rowStream = StreamSupport.stream(worksheet.spliterator(), false);

            final List<RespuestaPregunta> respuestas = new ArrayList<>();

            final AtomicBoolean isFundamental = new AtomicBoolean(false);
            final AtomicReference<Float> peso = new AtomicReference<>((float) 0);
            final AtomicReference<String> codigoPractica = new AtomicReference<>("");
            final AtomicReference<Practica> practica = new AtomicReference<>();
            rowStream.forEach(row -> {
                logger.info(row.toString());
                Stream<Cell> cellStream = StreamSupport.stream(row.spliterator(), false);
                List<String> cellString = cellStream.map(cell -> {
                    cell.setCellType(CellType.STRING);
                    return cell.getStringCellValue();
                }).collect(Collectors.toList());

                if (!cellString.get(0).isBlank() && !"Pregunta".equals(cellString.get(0))) {
                    final Pregunta p = preguntaRepository.findByCodigo(cellString.get(0));
                    practica.set(p.getPractica());
                    logger.debug(p.getPractica().toString() + " " + p.getPractica().getTipoPractica());
                    isFundamental.set(p.getPractica().getTipoPractica().getCodigo().equals("F"));
                    codigoPractica.set(p.getPractica().getCodigo());
                    peso.set(p.getPractica().getPeso());
                    final Boolean respuesta = cellString.get(2).isBlank() && cellString.get(3).isBlank() ? null : !cellString.get(2).isBlank();
                    final String observaciones = cellString.get(4);

                    RespuestaPregunta rp = RespuestaPregunta.builder().pregunta(p).evaluacion(evaluacion).respuesta(respuesta).observaciones(observaciones).build();
                    em.merge(rp);
                    respuestas.add(rp);
                }
            });
            final double n = respuestas.size();
            final double currentPI = respuestas.stream().filter(aux -> null != aux.getRespuesta() && aux.getRespuesta()).count() / n;
            final double currentPIP = currentPI * peso.get();
            evaluacionPracticas.add(ResultadoPracticaVO.builder().puntuacionIndividual(currentPI).puntuacionPonderada(currentPIP).tipoPractica(isFundamental.get() ? "F" : "C").codigoPractica(codigoPractica.get()).build());

            logger.info("PI:  " + currentPI + "%");
            logger.info("PIP:  " + currentPIP + "%");
            final ResultadoPractica rp = ResultadoPractica.builder().evaluacion(evaluacion).practica(practica.get()).resultadoIndividual(currentPI).resultadoPonderado(currentPIP).build();
            em.merge(rp);

        }
        return evaluacionPracticas;
    }

    private List<ResultadoDimensionVO> uploadDimensiones(Evaluacion evaluacion, List<ResultadoPracticaVO> evaluacionPracticas, double pesoFund, double pesoComp) {
        final List<ResultadoDimensionVO> resultadoDimensiones = new ArrayList<>();
        final List<DimensionPractica> dp = dimensionPracticaRepository.findActivos();
        final List<Dimension> dimensiones = StreamEx.of(dp).distinct(e -> e.getDimension().getCodigo()).map(DimensionPractica::getDimension).toList();

        for (Dimension dimension : dimensiones) {
            logger.info("Dimension: " + dimension.getCodigo());

            final List<String> codigoPracticas = dp.stream().filter(aux -> aux.getDimension().getCodigo().equals(dimension.getCodigo())).map(current -> current.getPractica().getCodigo()).collect(Collectors.toList());

            final List<ResultadoPracticaVO> practicasAsociadas = evaluacionPracticas.stream().filter(aux -> codigoPracticas.contains(aux.getCodigoPractica())).collect(Collectors.toList());

            final List<ResultadoPracticaVO> practicasFund = practicasAsociadas.stream().filter(aux -> aux.getTipoPractica().equals("F")).collect(Collectors.toList());
            final List<ResultadoPracticaVO> practicasComp = practicasAsociadas.stream().filter(aux -> aux.getTipoPractica().equals("C")).collect(Collectors.toList());

            final double pcdif = practicasFund.stream().mapToDouble(ResultadoPracticaVO::getPuntuacionIndividual).sum() / practicasFund.size();
            final double pcdic = practicasComp.stream().mapToDouble(ResultadoPracticaVO::getPuntuacionIndividual).sum() / practicasComp.size();
            final double pcdi = (pesoFund * pcdif) + (pesoComp * pcdic);
            final double resPonderado = pcdi * dimension.getPeso();
            resultadoDimensiones.add(ResultadoDimensionVO.builder().puntuacionIndividual(pcdi).puntuacionPonderada(resPonderado).codigoDimension(dimension.getCodigo()).build());

            final ResultadoDimension rd = ResultadoDimension.builder().evaluacion(evaluacion).dimension(dimension).resultadoIndividual(pcdi).resultadoPonderado(resPonderado).build();
            em.merge(rd);
            logger.info("pcdi:  " + pcdi + "%");

        }
        return resultadoDimensiones;
    }

    private List<ResultadoValorVO> uploadValores(Evaluacion evaluacion, List<ResultadoDimensionVO> resultadoDimensiones) {
        final List<DimensionValor> dv = dimensionValorRepository.findActivos();
        final List<Valor> valores = StreamEx.of(dv).distinct(e -> e.getValor().getCodigo()).map(DimensionValor::getValor).toList();

        final List<ResultadoValorVO> resultadoValores = new ArrayList<>();
        for (final Valor valor : valores) {
            logger.info("valor: " + valor.getCodigo());

            final List<String> codigoDimensiones = dv.stream().filter(aux -> aux.getValor().getCodigo().equals(valor.getCodigo())).map(current -> current.getDimension().getCodigo()).collect(Collectors.toList());
            final List<ResultadoDimensionVO> dimensionesAsociadas = resultadoDimensiones.stream().filter(aux -> codigoDimensiones.contains(aux.getCodigoDimension())).collect(Collectors.toList());

            final double pcvi = dimensionesAsociadas.stream().mapToDouble(ResultadoDimensionVO::getPuntuacionIndividual).sum() / dimensionesAsociadas.size();
            final double resPonderado = pcvi * valor.getPeso();

            resultadoValores.add(ResultadoValorVO.builder().puntuacionIndividual(pcvi).puntuacionPonderada(resPonderado).codigoValor(valor.getCodigo()).build());

            final ResultadoValor rv = ResultadoValor.builder().evaluacion(evaluacion).valor(valor).resultadoIndividual(pcvi).resultadoPonderado(resPonderado).build();
            em.merge(rv);
            logger.info("pcvi:  " + pcvi + "%");
        }
        return resultadoValores;
    }

    @Override
    public ResultadoEvaluacionVO getResultados(@NonNull final Long id) {
        final Evaluacion eval = evaluacionRepository.findById(id).get();

        final List<ResultadoPractica> rp = resultadoPracticaRepository.findResultados(id);
        final List<ResultadoDimension> rd = resultadoDimensionRepository.findResultados(id);
        final List<ResultadoValor> rv = resultadoValorRepository.findResultados(id);

        final List<PonderacionPractica> pesoPracticas = ponderacionPracticaRepository.findActivos();

        final double pesoComp = pesoPracticas.stream().filter(aux -> "C".equals(aux.getTipoPractica().getCodigo())).findFirst()
                .orElseThrow(() -> new NoSuchElementException("Práctica no encontrada"))
                .getValor();
        final double pesoFund = pesoPracticas.stream().filter(aux -> "F".equals(aux.getTipoPractica().getCodigo())).findFirst()
                .orElseThrow(() -> new NoSuchElementException("Práctica no encontrada"))
                .getValor();

        final List<ResultadoPracticaVO> resultadoPracticas = rp.stream().map(aux -> ResultadoPracticaVO.builder().idPractica(aux.getPractica().getId()).nombrePractica(aux.getPractica().getDescripcion()).codigoPractica(aux.getPractica().getCodigo()).tipoPractica(aux.getPractica().getTipoPractica().getCodigo()).puntuacionIndividual(GeneralUtil.decToPercentaje(100 * aux.getResultadoIndividual())).puntuacionPonderada(GeneralUtil.decToPercentaje(100 * aux.getResultadoPonderado())).build()).collect(Collectors.toList());

        final double pft = resultadoPracticas.stream().filter(practica -> practica.getTipoPractica().equals("F")).mapToDouble(ResultadoPracticaVO::getPuntuacionPonderada).sum();
        final double pct = resultadoPracticas.stream().filter(practica -> practica.getTipoPractica().equals("C")).mapToDouble(ResultadoPracticaVO::getPuntuacionPonderada).sum();
        final double ptcp = GeneralUtil.decToPercentaje((pesoFund * pft) + (pesoComp * pct));

        final List<ResultadoDimensionVO> resultadoDimensiones = rd.stream().map(aux -> ResultadoDimensionVO.builder().nombreDimension(aux.getDimension().getDescripcion()).codigoDimension(aux.getDimension().getCodigo()).puntuacionIndividual(GeneralUtil.decToPercentaje(100 * aux.getResultadoIndividual())).puntuacionPonderada(GeneralUtil.decToPercentaje(100 * aux.getResultadoPonderado())).build()).collect(Collectors.toList());

        final double pcdt = GeneralUtil.decToPercentaje(resultadoDimensiones.stream().mapToDouble(ResultadoDimensionVO::getPuntuacionPonderada).sum());

        final List<ResultadoValorVO> resultadoValores = rv.stream().map(aux -> ResultadoValorVO.builder().nombreValor(aux.getValor().getDescripcion()).codigoValor(aux.getValor().getCodigo()).puntuacionIndividual(GeneralUtil.decToPercentaje(100 * aux.getResultadoIndividual())).puntuacionPonderada(GeneralUtil.decToPercentaje(100 * aux.getResultadoPonderado())).build()).collect(Collectors.toList());

        final double pc_dev = GeneralUtil.decToPercentaje(resultadoValores.stream().mapToDouble(ResultadoValorVO::getPuntuacionPonderada).sum());

        return ResultadoEvaluacionVO.builder().idEvaluacion((eval.getId())).idEmpresa(eval.getEmpresa().getId()).nombreEmpresa(eval.getEmpresa().getNombre()).fechaEvaluacion(eval.getFechaEvaluacion()).resultadoPracticas(resultadoPracticas).resPracticasFund(pft).resPracticasComp(pct).resTotalPracticas(ptcp).resultadoDimensiones(resultadoDimensiones).resTotalDimensiones(pcdt).resultadoValores(resultadoValores).cumplimientoDevOps(pc_dev).build();
    }

    @Override
    public List<ResumenPreguntasVO> getPreguntasByPracticaId(final ResumenPreguntasRQ rq) {
        return respuestaPreguntaRepository.findByPracticaId(rq.getIdEvaluacion(), rq.getIdPractica());
    }

    @Override
    public Empresa uploadDummy() {
        try {
            final String file_name = "plantilla-evaluacion-estudio-caso-dummy.xlsx";
            final File file = new File(Objects.requireNonNull(getClass().getClassLoader().getResource(file_name)).getFile());
            final MultipartFile rq = new MockMultipartFile(file_name, new FileInputStream(file));
            return uploadFromApp(rq);
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public ResponseEntity<Resource> downloadFile(@NonNull final Long id) {
        try {
            final Optional<Evaluacion> evaluacion = evaluacionRepository.findById(id);
            if (evaluacion.isPresent()) {
                final byte[] file = s3Client.download(evaluacion.get().getRutaArchivo());
                final ByteArrayResource resource = new ByteArrayResource(file);

                final HttpHeaders headers = new HttpHeaders();
                headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + evaluacion.get().getRutaArchivo());
                if (evaluacion.get().getRutaArchivo().endsWith(".xls")) {
                    headers.add(HttpHeaders.CONTENT_TYPE, "application/vnd.ms-excel");
                } else if (evaluacion.get().getRutaArchivo().endsWith(".xlsx")) {
                    headers.add(HttpHeaders.CONTENT_TYPE, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                }

                return ResponseEntity.ok().headers(headers).body(resource);

            } else {
                return null;
            }
        } catch (IOException e) {
            return null;
        }
    }
}

