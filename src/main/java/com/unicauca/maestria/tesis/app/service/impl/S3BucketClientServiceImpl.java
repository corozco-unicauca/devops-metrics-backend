package com.unicauca.maestria.tesis.app.service.impl;

import com.amazonaws.AmazonClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.util.IOUtils;
import com.unicauca.maestria.tesis.app.service.S3BucketClientService;
import com.unicauca.maestria.tesis.app.util.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;

@Service
public class S3BucketClientServiceImpl implements S3BucketClientService {

    private final Logger logger = LoggerFactory.getLogger(S3BucketClientServiceImpl.class);

    @Value(Constants.BUCKET_NAME)
    private String bucketName;

    private final AmazonS3 s3Client;

    public S3BucketClientServiceImpl(AmazonS3 s3Client) {
        this.s3Client = s3Client;
    }

    public boolean folderExists(final String folderName) {
        final ListObjectsV2Result result = s3Client.listObjectsV2(bucketName, folderName);
        final List<S3ObjectSummary> objects = result.getObjectSummaries();
        return objects.stream().anyMatch(os -> os.getKey().endsWith("/"));
    }

    @Override
    public boolean fileExists(final String path) {
        try {
            return Optional.ofNullable(s3Client.getObject(bucketName, path)).isPresent();
        } catch (AmazonClientException e) {
            logger.info("S3BucketClientServiceImpl#fileExists - WARN - file doesn't exist {}.", path);
            return false;
        }
    }

    @Override
    public byte[] download(final String path) throws IOException {
        logger.info("S3BucketClientServiceImpl#download - INFO - start method");
        final S3Object s3Object = s3Client.getObject(bucketName, path);
        final S3ObjectInputStream inputStream = s3Object.getObjectContent();
        logger.info("S3BucketClientServiceImpl#download - INFO - end method");
        return IOUtils.toByteArray(inputStream);
    }

    @Override
    public void upload(final String path, final MultipartFile file) {
        try {
            final ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentLength(file.getSize());
            s3Client.putObject(bucketName, path, file.getInputStream(), metadata);
            logger.info("S3BucketClientServiceImpl#upload - INFO - end method");
        } catch (IOException e) {
            logger.info("S3BucketClientServiceImpl#upload - ERROR - error on upload file");
        }
    }

    @Override
    public void createFolder(final String folderName) {
        final ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(0);

        final InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
        final PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName,
                folderName + "/", emptyContent, metadata);
        s3Client.putObject(putObjectRequest);
        logger.info("S3BucketClientServiceImpl#createFolder - INFO - end method");

    }
}
