package com.unicauca.maestria.tesis.app.exception;

/**
 * AdminException
 * Thrown when there's an error related with the AdminController
 */
public class AdminException extends RuntimeException {

    private static final long serialVersionUID = 1L;

}