package com.unicauca.maestria.tesis.app.vo;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class ResultadoPracticaVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idPractica;

    private double puntuacionIndividual;

    private double puntuacionPonderada;

    private String codigoPractica;

    private String nombrePractica;

    private String tipoPractica;
}
