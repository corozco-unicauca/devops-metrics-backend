package com.unicauca.maestria.tesis.app.service;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


@Service
public interface S3BucketClientService {

    boolean folderExists(final String folderName);

    boolean fileExists(final String path);

    byte[] download(String path) throws IOException;

    void upload(final String path, MultipartFile file);

    void createFolder(final String folderName);

}
