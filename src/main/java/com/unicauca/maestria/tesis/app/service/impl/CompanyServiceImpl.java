package com.unicauca.maestria.tesis.app.service.impl;

import com.unicauca.maestria.tesis.app.entity.Empresa;
import com.unicauca.maestria.tesis.app.repository.EmpresaRepository;
import com.unicauca.maestria.tesis.app.service.CompanyService;
import lombok.AllArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@AllArgsConstructor
public class CompanyServiceImpl implements CompanyService {

    private EmpresaRepository empresaRepository;

    @Override
    public List<Empresa> findAllActive() {
        return empresaRepository.findActivos();
    }

    @Override
    public Empresa findById(@NonNull final Long id) {
        return empresaRepository.findById(id).orElse(null);
    }

}
