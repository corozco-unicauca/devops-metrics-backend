package com.unicauca.maestria.tesis.app.config;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.unicauca.maestria.tesis.app.util.Constants;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * S3Config
 * Configuration class to communicate with S3 bucket according to given keys
 */
@Configuration
public class S3Config {

    @Value(Constants.AWS_ACCESS_KEY)
    private String accessKey;
    @Value(Constants.AWS_SECRET_KEY)
    private String accessSecret;
    @Value(Constants.AWS_REGION)
    private String region;

    @Bean
    public AmazonS3 s3Client() {
        AWSCredentials credentials = new BasicAWSCredentials(accessKey, accessSecret);
        return AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(region)
                .build();
    }
}
