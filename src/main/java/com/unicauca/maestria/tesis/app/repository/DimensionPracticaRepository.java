package com.unicauca.maestria.tesis.app.repository;

import com.unicauca.maestria.tesis.app.entity.DimensionPractica;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DimensionPracticaRepository extends JpaRepository<DimensionPractica, Long> {

    @Query(value = "SELECT dp FROM dimension_practica dp JOIN dp.dimension dim JOIN dp.practica p " +
            "JOIN p.tipoPractica tp WHERE dp.activo = true ORDER BY dim.id ASC")
    List<DimensionPractica> findActivos();
}
