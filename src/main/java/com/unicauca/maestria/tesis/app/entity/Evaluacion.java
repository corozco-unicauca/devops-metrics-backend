package com.unicauca.maestria.tesis.app.entity;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@Entity(name = "evaluacion")
public class Evaluacion extends EntidadGeneral {

    @JoinColumn(name = "empresa_id", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Empresa empresa;

    @Column(name = "fecha_evaluacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEvaluacion;

    @Column(name = "resultado")
    private double resultado;

    @Column(name = "ruta_archivo")
    private String rutaArchivo;

    public Evaluacion() {
        super();
    }
}
