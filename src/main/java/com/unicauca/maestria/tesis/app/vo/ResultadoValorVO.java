package com.unicauca.maestria.tesis.app.vo;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class ResultadoValorVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private double puntuacionIndividual;

    private double puntuacionPonderada;

    private String codigoValor;

    private String nombreValor;
}
