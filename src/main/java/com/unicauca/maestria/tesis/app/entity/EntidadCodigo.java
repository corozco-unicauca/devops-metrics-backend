package com.unicauca.maestria.tesis.app.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@MappedSuperclass
public class EntidadCodigo extends EntidadGeneral {

    @Column(name = "codigo")
    private String codigo;

    public EntidadCodigo() {
    }

}