package com.unicauca.maestria.tesis.app.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
public class ResumenPreguntasVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String codigoPregunta;

    private String pregunta;

    private String respuestaSi;

    private String respuestaNo;

    private String observaciones;
}
