package com.unicauca.maestria.tesis.app.vo;

import lombok.Builder;
import lombok.Data;
import org.springframework.core.io.Resource;

import java.io.Serializable;

@Data
@Builder
public class DownloadFileVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String filename;
    private Resource file;
}
