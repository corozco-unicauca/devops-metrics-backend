package com.unicauca.maestria.tesis.app.util;

/**
 * Constants
 * Contains constants related to pse process and properties.
 */

public final class Constants {

    // AWS properties
    public static final String AWS_ACCESS_KEY = "${cloud.aws.credentials.access-key}";
    public static final String AWS_SECRET_KEY = "${cloud.aws.credentials.secret-key}";
    public static final String AWS_REGION = "${cloud.aws.region.static}";

    public static final String BUCKET_NAME = "${application.bucket.name}";
}
