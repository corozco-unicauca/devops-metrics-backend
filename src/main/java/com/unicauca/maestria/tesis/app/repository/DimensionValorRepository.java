package com.unicauca.maestria.tesis.app.repository;

import com.unicauca.maestria.tesis.app.entity.DimensionValor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DimensionValorRepository extends JpaRepository<DimensionValor, Long> {

    @Query(value = "SELECT dv FROM dimension_valor dv JOIN dv.dimension dim JOIN dv.valor v " +
            " WHERE dv.activo = true ORDER BY v.id ASC")
    List<DimensionValor> findActivos();
}
