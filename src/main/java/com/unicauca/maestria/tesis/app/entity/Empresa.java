package com.unicauca.maestria.tesis.app.entity;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@Entity(name = "empresa")
public class Empresa extends EntidadGeneral {

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "nit")
    private String nit;

    @Column(name = "direccion")
    private String direccion;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "contacto")
    private String contacto;

    public Empresa() {
        super();
    }
}
