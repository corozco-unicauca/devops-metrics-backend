package com.unicauca.maestria.tesis.app.entity;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@Entity(name = "tipo_practica")
public class TipoPractica extends EntidadCodigo {

    @Column(name = "descripcion")
    private String descripcion;

    public TipoPractica() {
        super();
    }
}
