package com.unicauca.maestria.tesis.app.entity;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@Entity(name = "resultado_dimension")
public class ResultadoDimension extends EntidadGeneral {

    @JoinColumn(name = "dimension_id", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Dimension dimension;

    @JoinColumn(name = "evaluacion_id", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Evaluacion evaluacion;

    @Column(name = "resultado_individual")
    private double resultadoIndividual;

    @Column(name = "resultado_ponderado")
    private double resultadoPonderado;

    public ResultadoDimension() {
        super();
    }
}
