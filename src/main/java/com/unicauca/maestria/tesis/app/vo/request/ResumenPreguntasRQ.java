package com.unicauca.maestria.tesis.app.vo.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ResumenPreguntasRQ implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idEvaluacion;

    private Long idPractica;
}
