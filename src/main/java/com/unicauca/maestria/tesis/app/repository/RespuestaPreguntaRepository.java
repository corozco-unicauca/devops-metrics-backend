package com.unicauca.maestria.tesis.app.repository;

import com.unicauca.maestria.tesis.app.entity.RespuestaPregunta;
import com.unicauca.maestria.tesis.app.vo.ResumenPreguntasVO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RespuestaPreguntaRepository extends JpaRepository<RespuestaPregunta, Long> {

    @Query(value = "SELECT new com.unicauca.maestria.tesis.app.vo.ResumenPreguntasVO(p.codigo, p.descripcion, CASE WHEN rp.respuesta = true THEN 'X' ELSE '' END, CASE WHEN rp.respuesta = false THEN 'X' ELSE '' END, rp.observaciones)" +
            " FROM respuesta_pregunta rp JOIN rp.evaluacion v JOIN rp.pregunta p JOIN p.practica prac where v.id =:idEvaluacion AND prac.id =:idPractica")
    List<ResumenPreguntasVO> findByPracticaId(Long idEvaluacion, Long idPractica);
}
