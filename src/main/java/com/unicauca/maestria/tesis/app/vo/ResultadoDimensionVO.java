package com.unicauca.maestria.tesis.app.vo;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class ResultadoDimensionVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private double puntuacionIndividual;

    private double puntuacionPonderada;

    private String codigoDimension;

    private String nombreDimension;
}
