package com.unicauca.maestria.tesis.app.repository;

import com.unicauca.maestria.tesis.app.entity.ResultadoDimension;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ResultadoDimensionRepository extends JpaRepository<ResultadoDimension, Long> {

    @Query(value = "SELECT rd FROM resultado_dimension rd JOIN rd.evaluacion e" +
            " JOIN rd.dimension d WHERE e.id=:idEvaluacion AND rd.activo = true ORDER BY d.id ASC")
    List<ResultadoDimension> findResultados(Long idEvaluacion);
}
