package com.unicauca.maestria.tesis.app.service;

import com.unicauca.maestria.tesis.app.entity.Empresa;
import com.unicauca.maestria.tesis.app.repository.EmpresaRepository;
import com.unicauca.maestria.tesis.app.service.impl.CompanyServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CompanyServiceImplTest {

    @Mock
    private EmpresaRepository empresaRepository;

    @InjectMocks
    private CompanyServiceImpl companyService;

    private Empresa empresa1;
    private Empresa empresa2;

    @BeforeEach
    void setUp() {
        empresa1 = new Empresa();
        empresa2 = new Empresa();
    }

    @Test
    void findAllActive() {
        when(empresaRepository.findActivos()).thenReturn(Arrays.asList(empresa1, empresa2));
        final List<Empresa> result = companyService.findAllActive();
        assertEquals(2, result.size());
        assertEquals(empresa1, result.get(0));
        assertEquals(empresa2, result.get(1));
    }

    @Test
    void findById_found() {
        when(empresaRepository.findById(1L)).thenReturn(Optional.of(empresa1));
        final Empresa result = companyService.findById(1L);
        assertEquals(empresa1, result);
    }

    @Test
    void findById_notFound() {
        when(empresaRepository.findById(1L)).thenReturn(Optional.empty());
        final Empresa result = companyService.findById(1L);
        assertNull(result);
    }
}

