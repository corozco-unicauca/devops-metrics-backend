package com.unicauca.maestria.tesis.app.service;

import com.unicauca.maestria.tesis.app.entity.Evaluacion;
import com.unicauca.maestria.tesis.app.repository.EvaluacionRepository;
import com.unicauca.maestria.tesis.app.repository.RespuestaPreguntaRepository;
import com.unicauca.maestria.tesis.app.service.impl.AssessmentServiceImpl;
import com.unicauca.maestria.tesis.app.vo.EvaluacionVO;
import com.unicauca.maestria.tesis.app.vo.ResumenPreguntasVO;
import com.unicauca.maestria.tesis.app.vo.request.ResumenPreguntasRQ;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AssessmentServiceImplTest {

    @InjectMocks
    private AssessmentServiceImpl assessmentService;

    @Mock
    private EvaluacionRepository evaluacionRepository;

    @Mock
    private RespuestaPreguntaRepository respuestaPreguntaRepository;

    @Mock
    private S3BucketClientService s3Client;

    @Test
    public void testFindByCompanyId() {
        Long companyId = 1L;

        Evaluacion evaluacion1 = new Evaluacion();
        evaluacion1.setId(1L);
        evaluacion1.setFechaCreacion(new Date());
        evaluacion1.setFechaEvaluacion(new Date());
        evaluacion1.setResultado(0.8);
        evaluacion1.setRutaArchivo("ruta/archivo1");

        Evaluacion evaluacion2 = new Evaluacion();
        evaluacion2.setId(2L);
        evaluacion2.setFechaCreacion(new Date());
        evaluacion2.setFechaEvaluacion(new Date());
        evaluacion2.setResultado(0.6);
        evaluacion2.setRutaArchivo("ruta/archivo2");

        when(evaluacionRepository.findByEmpresaId(companyId)).thenReturn(Arrays.asList(evaluacion1, evaluacion2));

        List<EvaluacionVO> result = assessmentService.findByCompanyId(companyId);

        assertThat(result).isNotNull();
        assertThat(result).hasSize(2);
        assertThat(result.get(0).getId()).isEqualTo(evaluacion1.getId());
        assertThat(result.get(0).getRutaArchivo()).isEqualTo(evaluacion1.getRutaArchivo());
        assertThat(result.get(0).getFechaEvaluacion()).isEqualTo(evaluacion1.getFechaEvaluacion());
        assertThat(result.get(1).getId()).isEqualTo(evaluacion2.getId());
        assertThat(result.get(1).getRutaArchivo()).isEqualTo(evaluacion2.getRutaArchivo());
        assertThat(result.get(1).getFechaEvaluacion()).isEqualTo(evaluacion2.getFechaEvaluacion());

        verify(evaluacionRepository, times(1)).findByEmpresaId(companyId);
    }

    @Test
    public void testGetPreguntasByPracticaId() {
        ResumenPreguntasRQ rq = new ResumenPreguntasRQ();
        rq.setIdEvaluacion(1L);
        rq.setIdPractica(1L);

        ResumenPreguntasVO resumenPreguntasVO1 = ResumenPreguntasVO.builder().build();
        ResumenPreguntasVO resumenPreguntasVO2 = ResumenPreguntasVO.builder().build();

        when(respuestaPreguntaRepository.findByPracticaId(rq.getIdEvaluacion(), rq.getIdPractica()))
                .thenReturn(Arrays.asList(resumenPreguntasVO1, resumenPreguntasVO2));

        List<ResumenPreguntasVO> result = assessmentService.getPreguntasByPracticaId(rq);

        assertThat(result).isNotNull();
        assertThat(result).hasSize(2);
        assertThat(result).containsExactly(resumenPreguntasVO1, resumenPreguntasVO2);

        verify(respuestaPreguntaRepository, times(1)).findByPracticaId(rq.getIdEvaluacion(), rq.getIdPractica());
    }

    @Test
    public void testDownloadFile() throws Exception {
        Long id = 1L;

        Evaluacion evaluacion = new Evaluacion();
        evaluacion.setId(id);
        evaluacion.setRutaArchivo("ruta/archivo.xlsx");

        byte[] fileContent = new byte[]{0, 1, 2};

        when(evaluacionRepository.findById(id)).thenReturn(Optional.of(evaluacion));
        when(s3Client.download(evaluacion.getRutaArchivo())).thenReturn(fileContent);

        ResponseEntity<Resource> result = assessmentService.downloadFile(id);

        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isEqualTo(new ByteArrayResource(fileContent));
        assertThat(result.getHeaders().getFirst(HttpHeaders.CONTENT_DISPOSITION)).isEqualTo("attachment; filename=" + evaluacion.getRutaArchivo());
        assertThat(result.getHeaders().getFirst(HttpHeaders.CONTENT_TYPE)).isEqualTo("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

        verify(evaluacionRepository, times(1)).findById(id);
        verify(s3Client, times(1)).download(evaluacion.getRutaArchivo());
    }

    @Test
    public void testDownloadFileWhenEvaluacionNotFound() {
        Long id = 1L;
        when(evaluacionRepository.findById(id)).thenReturn(Optional.empty());
        ResponseEntity<Resource> result = assessmentService.downloadFile(id);
        assertThat(result).isNull();
        verify(evaluacionRepository, times(1)).findById(id);
    }

    @Test
    public void testDownloadFileWhenExceptionThrown() throws Exception {
        Long id = 1L;

        Evaluacion evaluacion = new Evaluacion();
        evaluacion.setId(id);
        evaluacion.setRutaArchivo("ruta/archivo.xlsx");

        when(evaluacionRepository.findById(id)).thenReturn(Optional.of(evaluacion));
        when(s3Client.download(evaluacion.getRutaArchivo())).thenThrow(new IOException("Test exception"));

        ResponseEntity<Resource> result = assessmentService.downloadFile(id);
        assertThat(result).isNull();

        verify(evaluacionRepository, times(1)).findById(id);
        verify(s3Client, times(1)).download(evaluacion.getRutaArchivo());
    }
}
