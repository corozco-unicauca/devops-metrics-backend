package com.unicauca.maestria.tesis.app.service;

import com.amazonaws.AmazonClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import com.unicauca.maestria.tesis.app.service.impl.S3BucketClientServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class S3BucketClientServiceImplTest {

    @Mock
    private AmazonS3 s3Client;

    @InjectMocks
    private S3BucketClientServiceImpl s3BucketClientService;

    private static final String BUCKET_NAME = "test_bucket_name";

    @BeforeEach
    public void setup() {
        ReflectionTestUtils.setField(s3BucketClientService, "bucketName", BUCKET_NAME);
    }

    @Test
    public void testFolderExists() {
        final String folderName = "test-folder";
        final S3ObjectSummary summary = new S3ObjectSummary();
        summary.setKey(folderName + "/");

        final ListObjectsV2Result result = mock(ListObjectsV2Result.class);
        final List<S3ObjectSummary> objectSummaries = new ArrayList<>();
        objectSummaries.add(summary);

        when(s3Client.listObjectsV2(anyString(), anyString())).thenReturn(result);
        when(result.getObjectSummaries()).thenReturn(objectSummaries);
        assertTrue(s3BucketClientService.folderExists(folderName));
    }

    @Test
    public void testFileExists() {
        final String filePath = "test-path";
        when(s3Client.getObject(anyString(), anyString())).thenReturn(new S3Object());
        assertTrue(s3BucketClientService.fileExists(filePath));
    }

    @Test
    public void testFileNotExists() {
        final String filePath = "test-path";
        when(s3Client.getObject(anyString(), anyString())).thenThrow(new AmazonClientException("Error"));
        assertFalse(s3BucketClientService.fileExists(filePath));
    }

    @Test
    public void testDownload() throws IOException {
        final String filePath = "test-path";
        final S3Object s3Object = new S3Object();
        s3Object.setObjectContent(new S3ObjectInputStream(new ByteArrayInputStream("test-data".getBytes()), null));
        when(s3Client.getObject(anyString(), anyString())).thenReturn(s3Object);

        final byte[] content = s3BucketClientService.download(filePath);
        assertNotNull(content);
        assertEquals("test-data", new String(content));
    }

    @Test
    public void testUpload() throws IOException {
        final String filePath = "test-path";
        final MultipartFile file = mock(MultipartFile.class);

        when(file.getSize()).thenReturn(123L);
        when(file.getInputStream()).thenReturn(new ByteArrayInputStream("test-data".getBytes()));

        s3BucketClientService.upload(filePath, file);
        verify(s3Client, times(1)).putObject(anyString(), anyString(), any(InputStream.class), any(ObjectMetadata.class));
    }

    @Test
    public void testUpload_throwsException() throws IOException {
        MultipartFile file = mock(MultipartFile.class);
        when(file.getInputStream()).thenThrow(IOException.class);
        assertDoesNotThrow(() -> s3BucketClientService.upload("somepath", file));
    }

    @Test
    public void testCreateFolder() {
        final String folderName = "test-folder";
        s3BucketClientService.createFolder(folderName);
        verify(s3Client, times(1)).putObject(any(PutObjectRequest.class));
    }
}

