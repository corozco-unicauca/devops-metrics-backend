# README #

* Author: PhD(c). MSc. Eng. Carlos Eduardo Orozco
* Institution: Universidad del Cauca

### Pre-requisites ###

* Docker latest version
* Java 11+ version
* Maven latest version

### What is this repository for? ###

* Project created with the objective of supporting the process to evaluate the degree of adoption of the practices, dimensions and values ​​proposed in the DevOps Model through a reporting system that allows calculating and visualizing the information of interest to a consultant or professional who seeks to evaluate DevOps in software companies.
* Version 1.0

### How do I get set up? ###

* Execute: 
```sh
mvn clean install test package
```
* Execute:
```sh
docker compose up my-app
```
### Who do I talk to? ###

* Carlos Eduardo Orozco (carlosorozco@unicauca.edu.co)